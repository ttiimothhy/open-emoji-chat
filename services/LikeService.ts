import {Knex} from "knex";

export class LikeService {
    constructor(private knex: Knex) {}

    async targetUser(targetId: number, userId: number) {
        const txn = await this.knex.transaction(); // Demo
        try {
            let userInfo = await txn.select("*").from("users").where("id", targetId);
            let recordingReceived = await txn.select("recording", "emoji").from("like_pages").where({
                answer_user_id: targetId,
                target_user_id: userId,
            });
            let recordingSent = await txn.select("recording", "emoji").from("like_pages").where({
                answer_user_id: userId,
                target_user_id: targetId,
            });
            await txn.commit();
            return [userInfo, recordingReceived, recordingSent];
        } catch (e) {
            await txn.rollback();
            throw e;
        }
    }

    async like(userId: number, targetId: number, recording: string) {
        return await this.knex
            .insert({
                answer_user_id: userId,
                target_user_id: targetId,
                like_the_user: true,
                recording: recording,
            })
            .into("like_pages");
    }

    async addEmoji(recording: string, emoji: string) {
        const txn = await this.knex.transaction();
        try {
            await txn("like_pages").update({emoji: emoji}).where("recording", recording);
            // check last emoji
            let lastSentUser = await txn.select("target_user_id").from("like_pages").where("recording", recording);
            if (lastSentUser.length > 0) {
                let lastReceiveUser = await txn
                    .select("answer_user_id")
                    .from("like_pages")
                    .where("recording", recording);
                let lastEmoji = (
                    await txn
                        .select("emoji")
                        .from("like_pages")
                        .where("target_user_id", lastReceiveUser[0].answer_user_id)
                )[0].emoji;
                let success = ["Happy", "Neutral", "Surprised"];
                if (success.includes(emoji) && success.includes(lastEmoji)) {
                    await txn
                        .insert([
                            {
                                user_id: lastSentUser[0].target_user_id,
                                friend_id: lastReceiveUser[0].answer_user_id,
                            },
                            {
                                user_id: lastReceiveUser[0].answer_user_id,
                                friend_id: lastSentUser[0].target_user_id,
                            },
                        ])
                        .into("friends");
                }
            }
            await txn.commit();
        } catch (e) {
            await txn.rollback();
            throw e;
        }
    }

    async reject(userId: number, targetId: number) {
        return await this.knex
            .insert({
                answer_user_id: userId,
                target_user_id: targetId,
                like_the_user: false,
            })
            .into("like_pages");
    }
}
