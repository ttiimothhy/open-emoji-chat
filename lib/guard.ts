import express, {Request, Response, NextFunction} from "express";
import path from "path";

export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
    if (req.session["user"]) {
        next();
    } else {
        res.json({message: "Unauthorized"});
    }
}

export function isLoggedIn(req: Request, res: Response, next: express.NextFunction) {
    if (req.session["user"]) {
        next();
    } else {
        res.sendFile(path.resolve("./public/html/404.html"));
    }
}

export function isadmin(req: Request, res: Response, next:NextFunction){
	if (req.session["user"].username === "admin@emoji") {
        next();
    } else {
        res.sendFile(path.resolve("./public/html/404.html"));
    }
}
