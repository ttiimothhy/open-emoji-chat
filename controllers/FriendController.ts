import {Request, Response} from "express";
import {logger} from "../lib/logger";
import {FriendService} from "../services/FriendService";
import {Server as SocketIO} from "socket.io";

export class FriendController {
    constructor(private friendService: FriendService, private io: SocketIO) {}

    getFriendList = async (req: Request, res: Response) => {
        try {
            let user_id = req.session["user"].id;
            if (user_id) {
                let friendList = await this.friendService.friendList(user_id);
                res.json(friendList);
                logger.debug(friendList);
            } else {
                res.status(400).json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    getChat = async (req: Request, res: Response) => {
        try {
            const friendID = parseInt(req.params.id);
            let user_id = req.session["user"].id;
            if (user_id > 0 && friendID > 0) {
                let chat = await this.friendService.chatHistory(user_id, friendID);
                res.json(chat);
            } else {
                res.json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    sendMessage = async (req: Request, res: Response) => {
        try {
            let recordingSet = req.body;
            let userID = req.session["user"].id;
            const targetID = parseInt(req.params.id);
            if (req.file) {
                recordingSet = req.file.filename;
            }
            await this.friendService.sendMessage(userID, targetID, recordingSet);
            res.json({success: true});
            this.io.emit("new-message");
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    searchMessage = async (req: Request, res: Response) => {
        try {
            let user_id = req.session["user"].id;
            let searchWord = req.body;
            await this.friendService.searchMessage(user_id, searchWord);
            res.status(200).json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
