import {Request, Response} from "express";
import {logger} from "../lib/logger";
import {GroupService} from "../services/GroupService";
import {Server as SocketIO} from "socket.io";

export class GroupController {
    constructor(private groupService: GroupService, private io: SocketIO) {}

    addGroup = async (req: Request, res: Response) => {
        try {
            let {group_name, description, group_icon} = req.body;
            let userId = req.session["user"].id;

            if (req.body) {
                if (req.file) {
                    group_icon = req.file.filename;
                }
                await this.groupService.addGroup(group_name, description, group_icon, userId);
                res.json({success: true});
            } else {
                res.json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    groupList = async (req: Request, res: Response) => {
        try {
            let groupList = await this.groupService.groupList();
            res.json(groupList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    groupWaitInfoList = async (req: Request, res: Response) => {
        try {
            let id = req.session["user"].id;
            let groupWaitInfoList = await this.groupService.groupWaitInfoList(id);
            logger.debug(JSON.stringify(groupWaitInfoList, null, 4));
            res.json(groupWaitInfoList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    checkInGroup = async (req: Request, res: Response) => {
        try {
            let id = req.session["user"].id;
            let selfGroupList = await this.groupService.checkInGroup(id);
            res.json(selfGroupList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    checkInSpecificGroup = async (req: Request, res: Response) => {
        try {
            let member_id = req.session["user"].id;
            let group_id = parseInt(req.params.id);
            let selfGroupList = await this.groupService.checkInSpecificGroup(member_id, group_id);
            res.json(selfGroupList);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    joinGroup = async (req: Request, res: Response) => {
        try {
            let group_id = req.body.index;
            let member_id = req.session["user"].id;
            await this.groupService.joinGroup(group_id, member_id);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    groupName = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);
            let groupName = (await this.groupService.groupName(id))[0];
            logger.debug(JSON.stringify(groupName, null, 4));
            res.json(groupName);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    groupInfo = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);
            let currentUserId = req.session["user"].id;
            let groupInfo = await this.groupService.groupInfo(id);
            res.json({currentUserId: currentUserId, groupInfo: groupInfo});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    groupWaitInfo = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);
            let member_id = req.session["user"].id;
            let groupWaitInfo = await this.groupService.groupWaitInfo(id);
            let adminInfo = await this.groupService.adminInfo(id, member_id);
            logger.debug(JSON.stringify(groupWaitInfo, null, 4));
            res.json({admin: adminInfo, groupWaitInfo: groupWaitInfo});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    getGroupChat = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);
            let groupMessage = await this.groupService.getGroupChat(id);
            res.json({currentUserId: req.session["user"].id, groupMessage: groupMessage});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    checkAdmin = async (req: Request, res: Response) => {
        try {
            let group_id = parseInt(req.params.id);
            let member_id = req.session["user"].id;
            let admin = (await this.groupService.checkAdmin(group_id, member_id))[0];
            res.json(admin);
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    addAdmin = async (req: Request, res: Response) => {
        try {
            let group_id = parseInt(req.params.id);
            let addAdminArray = req.body.addAdminArray;
            await this.groupService.addAdmin(group_id, addAdminArray);
			this.io.emit("update-admin");
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    addToGroup = async (req: Request, res: Response) => {
        try {
            let group_id = parseInt(req.params.id);
            let member_id = req.body.index;
            await this.groupService.addToGroup(group_id, member_id);
			this.io.emit("update-group");
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    changeGroup = async (req: Request, res: Response) => {
        try {
            let {group_name, description, group_icon} = req.body;
            let group_id = parseInt(req.params.id);
            if (req.body) {
                if (req.file) {
                    group_icon = req.file.filename;
                }
                await this.groupService.changeGroup(group_name, description, group_icon, group_id);
                res.json({success: true});
            } else {
                res.json({success: false});
            }
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    deleteFromWaiting = async (req: Request, res: Response) => {
        try {
            let group_id = parseInt(req.params.id);
            let member_id = req.body.index;
            await this.groupService.deleteFromWaiting(group_id, member_id);
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };

    sendGroupMessage = async (req: Request, res: Response) => {
        try {
            let content = req.body;
            logger.debug(JSON.stringify(content, null, 4));
            let group_id = parseInt(req.params.id);
            let member_id = req.session["user"].id;
            if (req.file) {
                content = req.file.filename;
            }
            await this.groupService.sendGroupMessage(member_id, group_id, content);
			this.io.emit("new-message");
            res.json({success: true});
        } catch (e) {
            logger.error(`path:${req.path} method:${req.method}`);
            logger.error(e);
            res.status(500).json({message: "internal server error"});
        }
    };
}
