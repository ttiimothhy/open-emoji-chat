import {Request, Response} from "express";
import {Knex} from "knex";
import {FindService} from "../../services/FindService";
import {FindController} from "../FindController";

describe("UserController", () => {
    let findController: FindController;
    let findService: FindService;
    let req: Request;
    let res: Response;

    beforeEach(() => {
        // 咁樣無問題因為我們馬上mock晒佢啲method
        findService = new FindService({} as Knex);
        // 你可以選擇return其他data
        jest.spyOn(findService, "availableUser").mockImplementation(() =>
            Promise.resolve([
                {
                    id: 1,
                    username: "gordon",
                    password: "tecky",
                    created_at: new Date("2021-04-19"),
                    updated_at: new Date("2021-04-19"),
                },
            ])
        );

        findController = new FindController(findService);

        req = ({
            body: {},
            query: {},
            params: {},
            session: {
                user: {
                    username: "gordon",
                },
            },
        } as any) as Request;

        res = ({
            json: jest.fn((obj: any) => null),
            status: jest.fn((statusCode: number) => res),
        } as any) as Response;
    });

    it("should show available user", async () => {
        await findController.showAvailable(req, res);
        expect(res.json).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith([
			{
				id: 1,
				username: "gordon",
				password: "tecky",
				created_at: new Date("2021-04-19"),
				updated_at: new Date("2021-04-19"),
			},
		]);
    });
});
