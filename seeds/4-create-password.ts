import {Knex} from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Inserts seed entries
    await knex("passwords").where("username", "ttiimmothhy").del();
    const users = await knex.select("*").from("passwords").where("username", "ttiimmothhy");
    if (users.length === 0) {
        await knex
            .insert({
                username: "ttiimmothhy",
                password: "ttiimmothhy",
            })
            .into("passwords");
    }
}
