let uploadImageBtn = document.querySelector("#output_image");
let successMessage = document.querySelector(".error");

function preview_image(event) {
    let reader = new FileReader();
    reader.onload = function () {
        let output = document.getElementById("output_image");
        output.type = "image";
        output.src = reader.result;
    };
    reader.readAsDataURL(event.currentTarget.files[0]);
}
uploadImageBtn.addEventListener("click", function () {
    document.querySelector("#original-upload-btn").click();
});

document.querySelector("#createGroup").addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.currentTarget;
    const formData = new FormData();
    formData.append("group_name", form.groupName.value);
    formData.append("description", form.groupText.value);
    if (form.groupIcon.files[0]) {
        formData.append("group_icon", form.groupIcon.files[0]);
    }

    const res = await fetch("/createGroup", {
        method: "post",
        body: formData, //formData唔需要Content-Type 果個header
    });

    if (res.status === 200) {
        successMessage.innerHTML = /*html*/ `<div class="sign-up-error">成功創建</div>`;
        form.reset();
        setTimeout(function () {
            window.location.href = "group.html";
        }, 1000);
    }
});
