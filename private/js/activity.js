import {currentLocation} from "./updateLocation.js";
const displayBy = document.querySelectorAll(".display-by");
const myActivity = document.querySelector("#myActivity");
const joinActivity = document.querySelector("#joinActivity");
const activityRecord = document.querySelector("#activityRecord");
const mainContainer = document.querySelector(".main-container");
let joinActivities;
let confirmActivities;
let completeActivity;
let cancelActivity;
let target_Array;
let genderIcon;
let socket;

function initSocket() {
    socket = io.connect();
}
initSocket();

socket.on("addNewActivity", async function () {
    if (parseInt(joinActivity.getAttribute("data-ascension")) === 1) {
        await loadAllActivity();
    }
});
socket.on("joinActivity", async function () {
    if (parseInt(myActivity.getAttribute("data-ascension")) === 1) {
        await toConfirmActivity();
        await waitingForConfirmActivity();
    }
    if (parseInt(joinActivity.getAttribute("data-ascension")) === 1) {
        await loadAllActivity();
    }
});
socket.on("confirmApplication", async function () {
    if (parseInt(myActivity.getAttribute("data-ascension")) === 1) {
        await appointedActivities();
        await toConfirmActivity();
        await waitingForConfirmActivity();
    }
    if (parseInt(joinActivity.getAttribute("data-ascension")) === 1) {
        await loadAllActivity();
    }
});
socket.on("completeActivity", async function () {
    if (parseInt(myActivity.getAttribute("data-ascension")) === 1) {
        await appointedActivities();
    }
    if (parseInt(activityRecord.getAttribute("data-ascension")) === 1) {
        await loadActivityHistory();
    }
});
socket.on("cancelActivities", async function () {
    if (parseInt(myActivity.getAttribute("data-ascension")) === 1) {
        await appointedActivities();
    }
    if (parseInt(activityRecord.getAttribute("data-ascension")) === 1) {
        await loadActivityHistory();
    }
});

// page1
// appointed
async function appointedActivities() {
    const res = await fetch("/appointedActivities");
    const result = await res.json();
    let activities = result[0];
    let userId = result[1];
    mainContainer.innerHTML = /*html*/ `<div class="available-dating"> <div>進行中的約會</div></div >`;
    if (activities.length === 0) {
        mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有約會</div>`;
    } else {
        for (let activity of activities) {
            if (activity.participant_id[0] === userId) {
                target_Array = 1;
            } else {
                target_Array = 0;
            }
            switch (activity.gender[target_Array]) {
                case 1:
                    genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                    break;
                case 2:
                    genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                    break;
                case 3:
                    genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                    break;
            }
            mainContainer.innerHTML += /*html*/ `
            <div class="col-12">
                <div class="col-12 available-dating-content">
                    <div>約會名稱：${activity.activity_title}</div>
                    <div>約會日期：${activity.activity_date}</div>
                    <div>約會內容：<div class="activity-text-color">${activity.activity_content}</div></div>
                </div>
                <div class="join-user">參與用戶：</div>
            </div>
            <div class="select-dating-single">
                <div class="select-dating-box">
                    <div>
                        <img class="icon-small" src="../uploads/${activity.icon[target_Array]}" />
                    </div>
                    <div class="select-dating-info">
                        <div class="display-name">${activity.display_name[target_Array]}</div>
                        <div class="gender-age">
                            <div>${genderIcon}</div>
                            <div>${2021 - new Date(activity.date_of_birth[target_Array]).getFullYear()}</div>
                        </div>
                        <div class="description">${activity.description[target_Array]}</div>
                    </div>
                </div>
                <div class="dating-matched-select">
                    <div class="select-dating-matched select-date-chat" clickable ="1">
                        <a href="activitychat.html?activityId=${activity.activities_id}&userId=${
                activity.participant_id[target_Array]
            }"><div>開始聊天</div></a>
                    </div>
                    <div class="select-dating-matched select-date-completed" clickable ="1" activityId=${
                        activity.activities_id
                    }>
                        <div>完成約會</div>
                    </div>
                    </div>
                    <div class="select-dating-matched select-date-cancel" clickable ="1" activityId=${
                        activity.activities_id
                    }>
                        <div>取消約會</div>
                    </div>
            </div>`;
        }
        setTimeout(() => {
            completeActivity = document.querySelectorAll(".select-date-completed");
            cancelActivity = document.querySelectorAll(".select-date-cancel");
            completeActivities();
            cancelActivities();
        }, 1000);
    }
}

// load my activity
async function toConfirmActivity() {
    const res = await fetch("/toConfirmActivity");
    const activities = await res.json();
    mainContainer.innerHTML += /*html*/ `
    <div class="available-dating"><div>需確認的約會</div></div>`;
    if (activities.length === 0) {
        mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有約會</div>`;
    } else {
        for (let activity of activities) {
            mainContainer.innerHTML += /*html*/ `
            <div class="col-12">
                <div class="col-12 available-dating-content">
                    <div>約會名稱：${activity.activity_title}</div>
                    <div>約會日期：${activity.activity_date}</div>
                    <div>約會內容：<div class="activity-text-color">${activity.activity_content}</div></div>
                </div>
                <div class="join-user">參與用戶：</div>
            </div>`;
            if (activity.participant_id.length === 1) {
                mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有對象申請</div>`;
            } else {
                for (let i = 1; i < activity.participant_id.length; i++) {
                    let genderIcon;
                    if (parseInt(activity.gender[i]) === 1) {
                        genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                    } else if (parseInt(activity.gender[i]) === 2) {
                        genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                    } else if (parseInt(activity.gender[i]) === 3) {
                        genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                    }
                    mainContainer.innerHTML += /*html*/ `
					<div class="select-dating">
						<div>
							<img class="icon-small" src="../uploads/${activity.icon[i]}" />
						</div>
						<div class="select-dating-info">
							<div class="display-flex">
								<div class="display-name">${activity.display_name[i]}</div>
								<div class="gender-age">
									<div>${genderIcon}</div>
									<div>${2021 - new Date(activity.date_of_birth[i]).getFullYear()}</div>
								</div>
							</div>
							<div class="description">${activity.description[i]}</div>
						</div>
						<div class="select-dating-confirmUser" clickable='1' userID="${activity.participant_id[i]}" activityID="${
                        activity.activities_id
                    }">
							<div class='confirmBtn'>確認</div>
						</div>
					</div>`;
                }
            }
        }
        setTimeout(() => {
            confirmActivities = document.querySelectorAll(".select-dating-confirmUser");
            confirmApplication();
        }, 1000);
    }
}

async function waitingForConfirmActivity() {
    const res = await fetch("/waitingActivity");
    const activities = await res.json();
    let genderIcon;
    mainContainer.innerHTML += /*html*/ `<div class="available-dating"><div>待確認的約會</div></div>`;
    if (activities.length === 0) {
        mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有約會</div>`;
    } else {
        for (let activity of activities) {
            switch (activity.gender) {
                case 1:
                    genderIcon = `<i class="fas fa-mars"></i>`;
                    break;
                case 2:
                    genderIcon = `<i class="fas fa-venus"></i>`;
                    break;
                case 3:
                    genderIcon = `<i class="fas fa-transgender"></i>`;
                    break;
            }
            mainContainer.innerHTML += /*html*/ `
            <div class="col-12">
                <div class="col-12 available-dating-content">
                    <div>約會名稱：${activity.activity_title}</div>
                    <div>約會日期：${activity.activity_date}</div>
                    <div>約會內容：<div class="activity-text-color">${activity.activity_content}</div></div>
                </div>
                <div class="join-user">發起用戶：</div>
            </div>
            <div class="select-dating-single">
                <div class="select-dating-box">
                    <div>
                        <img class="icon-small" src="../uploads/${activity.icon}" />
                    </div>
                    <div class="select-dating-info">
                        <div class="display-name">${activity.display_name}</div>
                        <div class="gender-age">
                            <div>${genderIcon}</div>
                            <div>${2021 - new Date(activity.date_of_birth).getFullYear()}</div>
                        </div>
                        <div class="description">${activity.description}</div>
                    </div>
                </div>
            </div>`;
        }
    }
}

// page2
// load all activity
async function loadAllActivity() {
    const res = await fetch("/activityList");
    const result = await res.json();
    if (res.status === 200) {
        mainContainer.innerHTML = /*html*/ `
		<div class="available-dating">
			<div>可參與的約會</div>
		</div>`;

        if (result.length > 0) {
            for (let i = 0; i < result.length; i++) {
                let genderIcon;
                switch (result[i].gender) {
                    case 1:
                        genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                        break;
                    case 2:
                        genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                        break;
                    case 3:
                        genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                        break;
                }
                mainContainer.innerHTML += /*html*/ `
                <div class="select-dating" >
                    <div>
                        <img class="icon-small" src="../uploads/${result[i].icon}" />
                    </div>
                    <div class="select-dating-info">
                        <div class="display-flex">
                            <div class="display-name">${result[i].display_name}</div>
                            <div class="gender-age">
                                <div>${genderIcon}</div>
                                <div>${2021 - new Date(result[i].date_of_birth).getFullYear()}</div>
                            </div>
                        </div>
                        <div class="description">${result[i].description}</div>
                    </div>
                    <div class="select-dating-content">約會主題：${result[i].activity_title}</div>
                    <div class="select-dating-content">約會日期：${result[i].activity_date}</div>
                    <div class="select-dating-content">約會內容：${result[i].activity_content}</div>
                    <div class="select-dating-join" clickable='1' activityID='${result[i].activities_id}'>
                        <div class='confirmBtn' >參加</div>
                    </div>
                </div>`;
            }
        } else {
            mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有約會</div>`;
        }
        joinActivities = document.querySelectorAll(".select-dating-join");
        sendApplication();
    }
}

// page3
async function loadActivityHistory() {
    const res = await fetch("/pastActivity");
    const result = await res.json();
    let success = result[0];
    let fail = result[1];
    let userId = result[2];
    mainContainer.innerHTML = /*html*/ `<div class="available-dating"><div>成功的約會</div></div>`;
    if (success.length === 0) {
        mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有約會</div>`;
    } else {
        for (let activity of success) {
            let target_Array;
            if (activity.participant_id[0] === userId) {
                target_Array = 1;
            } else {
                target_Array = 0;
            }
            switch (activity.gender[target_Array]) {
                case 1:
                    genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                    break;
                case 2:
                    genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                    break;
                case 3:
                    genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                    break;
            }
            mainContainer.innerHTML += /*html*/ `
            <div class="col-12">
                <div class="col-12 available-dating-content">
                    <div>約會名稱：${activity.activity_title}</div>
                    <div>約會日期：${activity.activity_date}</div>
                    <div>約會內容：<div class="activity-text-color">${activity.activity_content}</div></div>
                </div>
                <div class="join-user">參與用戶：</div>
            </div>
            <div class="select-dating-single">
                <div class="select-dating-box">
                    <div>
                        <img class="icon-small" src="../uploads/${activity.icon[target_Array]}" />
                    </div>
                    <div class="select-dating-info">
                        <div class="display-name">${activity.display_name[target_Array]}</div>
                        <div class="gender-age">
                            <div>${genderIcon}</div>
                            <div>${2021 - new Date(activity.date_of_birth[target_Array]).getFullYear()}</div>
                        </div>
                        <div class="description">${activity.description[target_Array]}</div>
                    </div>
                </div>
                <div class="dating-matched-select">
                    <div class="select-dating-matched select-date-confirm">
                        <a href="activitychat.html?activityId=${activity.activities_id}&userId=${
                activity.participant_id[target_Array]
            }"><div>繼續聊天</div></a>
                    </div>
                </div>
            </div>`;
        }
    }

    mainContainer.innerHTML += /*html*/ `
    <div class="available-dating"><div>失敗的約會</div></div>`;
    if (fail.length === 0) {
        mainContainer.innerHTML += /*html*/ `<div class="select-friend">暫時未有約會</div>`;
    } else {
        for (let activity of fail) {
            let target_Array;
            if (activity.participant_id[0] === userId) {
                target_Array = 1;
            } else {
                target_Array = 0;
            }
            switch (activity.gender[target_Array]) {
                case 1:
                    genderIcon = /*html*/ `<i class="fas fa-mars"></i>`;
                    break;
                case 2:
                    genderIcon = /*html*/ `<i class="fas fa-venus"></i>`;
                    break;
                case 3:
                    genderIcon = /*html*/ `<i class="fas fa-transgender"></i>`;
                    break;
            }
            mainContainer.innerHTML += /*html*/ `
            <div class="available-dating-content-other"><div>約會名稱：${activity.activity_title}</div></div>
            <div class="col-4 available-dating-content-other"><div>約會日期：<div class="activity-date">${activity.activity_date}</div></div></div>
            <div class="available-dating-content-other"><div>約會內容：${activity.activity_content}</div></div>`;
        }
    }
}

// send application
async function sendApplication() {
    joinActivities.forEach((element) => {
        element.addEventListener("click", async () => {
            if (parseInt(element.getAttribute("clickable")) === 1) {
                let activityID = element.getAttribute("activityID");
                const res = await fetch("/joinActivity/" + activityID);
                if (res.status === 200) {
                    element.innerHTML = /*html*/ `<div class='approving-activities'> 待審批</div > `;
                    element.setAttribute("clickable", "0");
                }
            }
        });
    });
}

// confirm application
async function confirmApplication() {
    confirmActivities.forEach((element) => {
        element.addEventListener("click", async () => {
            if (parseInt(element.getAttribute("clickable")) === 1) {
                let userID = element.getAttribute("userID");
                let activityID = element.getAttribute("activityID");
                const res = await fetch("/confirmApplication/" + userID + "/" + activityID);
                if (res.status === 200) {
                    element.innerHTML = /*html*/ `<div class='approving-activities'>已確認</div > `;
                    element.setAttribute("clickable", "0");
                }
            }
        });
    });
}

// complete activities
async function completeActivities() {
    completeActivity.forEach((element) => {
        element.addEventListener("click", async () => {
            if (parseInt(element.getAttribute("clickable")) === 1) {
                let activityId = element.getAttribute("activityId");
                const res = await fetch("/completeActivity/" + activityId);
                if (res.status === 200) {
                    element.innerHTML = /*html*/ `<div>已確認</div > `;
                    element.setAttribute("clickable", "0");
                }
            }
        });
    });
}

// complete activities
async function cancelActivities() {
    cancelActivity.forEach((element) => {
        element.addEventListener("click", async () => {
            if (parseInt(element.getAttribute("clickable")) === 1) {
                let activityId = element.getAttribute("activityId");
                const res = await fetch("/cancelActivities/" + activityId);
                if (res.status === 200) {
                    element.innerHTML = /*html*/ `<div>已確認</div > `;
                    element.setAttribute("clickable", "0");
                }
            }
        });
    });
}

function setDisplay() {
    displayBy.forEach((element) => {
        element.addEventListener("click", () => {
            const removedArray = [...displayBy].filter((filtering) => {
                return filtering !== element;
            });
            removedArray.forEach((element) => {
                element.setAttribute("data-ascension", "0");
                element.classList.remove("active");
            });
            if (parseInt(element.getAttribute("data-ascension")) === 1) {
                element.setAttribute("data-ascension", "-1");
            } else {
                element.setAttribute("data-ascension", "1");
            }
            element.classList.add("active");
            sortBy();
        });
    });
}

async function sortBy() {
    if (parseInt(myActivity.getAttribute("data-ascension")) === 1) {
        await appointedActivities();
        await toConfirmActivity();
        await waitingForConfirmActivity();
    }
    if (parseInt(joinActivity.getAttribute("data-ascension")) === 1) {
        await loadAllActivity();
    }
    if (parseInt(activityRecord.getAttribute("data-ascension")) === 1) {
        await loadActivityHistory();
    }
}

window.onload = async function () {
    currentLocation();
    setDisplay();
    sortBy();
};
