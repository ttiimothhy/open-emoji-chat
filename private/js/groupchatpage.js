import {
    startRecording,
    stopRecording,
    getChatHistory,
    socket,
    urlID,
    recordButton,
    stopButton,
} from "./groupchatroom.js";
let groupName = document.querySelector(".group-name");
let groupIcon = document.querySelector(".group-icon");
let groupDescription = document.querySelector(".group-message-new");
let groupMemberNumber = document.querySelector(".number");
let groupWaitingMemberNumber = document.querySelector(".waitNumber");
let groupMemberList = document.querySelector(".member");
let groupWaitingMemberList = document.querySelector(".waiting-member");
let addAdmin = document.querySelector(".add-admin");
let addAdminButton = document.querySelector(".add-admin-button");
let changeGroupButton = document.querySelector(".change-group-button");
let addAdminDecision = 0;

socket.on("new-message", async function () {
    await getChatHistory();
    let bottom = document.querySelector(".chat-area");
    bottom.scrollTop = bottom.scrollHeight;
});
socket.on("update-admin", async function () {
    await groupInfo();
});
socket.on("update-group", async function () {
    await groupInfo();
});

$(document).ready(function () {
    $("#sidebarCollapse").on("click", function () {
        $("#sidebar").toggleClass("active");
    });
});

async function checkInGroup() {
    const res = await fetch("/checkInSpecificGroup/" + urlID);
    const result = await res.json();
    return result;
}

async function findGroupName() {
    const res = await fetch("/groupName/" + urlID);
    const result = await res.json();
    groupName.innerHTML = /*html*/ `${result.group_name}`;
    groupIcon.innerHTML = /*html*/ `<img src="../uploads/${result.group_icon}">`;
    groupDescription.innerHTML = /*html*/ `${result.description}`;
    if (!result.group_icon) {
        groupIcon.classList.add("nullIcon");
        groupIcon.innerHTML = "";
    }
}

async function groupInfo() {
    const res = await fetch("/groupInfo/" + urlID);
    const result = await res.json();
    let memberInfo = result.groupInfo;
    const waitRes = await fetch("/groupWaitInfo/" + urlID);
    const waitResult = await waitRes.json();
    let waitInfo = waitResult.groupWaitInfo;

    groupMemberNumber.innerText = /*html*/ `${memberInfo.length}/250`;
    groupMemberList.innerHTML = /*html*/ ``;
    for (let i = 0; i < memberInfo.length; i++) {
        groupMemberList.innerHTML += /*html*/ `
		<div class="group-member-info">
			<img class="group-member-icon" src="../uploads/${memberInfo[i].icon}">
			<div class="adminName">
				<div class="flexName">
					<div class="group-member-name">${memberInfo[i].display_name}</div>
					<div class="admin-checkbox" index="${memberInfo[i].member_id}"></div>
				</div>
				${
                    memberInfo[i].is_admin
                        ? `<div class='admin'>${
                              result.currentUserId === memberInfo[i].member_id ? "<span class='you'>(you)</span>" : ""
                          }管理員</div>`
                        : `${
                              result.currentUserId === memberInfo[i].member_id
                                  ? "<div class='admin'><span class='you'>(you)</span></div>"
                                  : "<div class='admin'></div>"
                          }`
                }
			</div>
		</div>`;
    }

    let addAdminCheckboxes = document.querySelectorAll(".admin-checkbox");
    let whetherIsAdmin = document.querySelectorAll(".admin");
    addAdmin.addEventListener("click", async function () {
        const res = await fetch("/checkAdmin/" + urlID);
        const result = await res.json();
        if (addAdminDecision === 0) {
            if (result.is_admin) {
                addAdminButton.innerHTML = /*html*/ `<input type="button" id="add-admin-button" value="確認" />`;
                for (let i = 0; i < addAdminCheckboxes.length; i++) {
                    let index = parseInt(addAdminCheckboxes[i].getAttribute("index"));
                    if (!whetherIsAdmin[i].innerText.includes("管理員")) {
                        addAdminCheckboxes[
                            i
                        ].innerHTML = /*html*/ `<input type="checkbox" index=${index} class="not-admin-checkbox">`;
                    }
                }
                let whetherCheckboxes = document.querySelectorAll(".not-admin-checkbox");
                addAdminButton.addEventListener("click", async function () {
                    let addAdminArray = [];
                    for (let i = 0; i < whetherCheckboxes.length; i++) {
                        let index = parseInt(whetherCheckboxes[i].getAttribute("index"));
                        if (whetherCheckboxes[i].checked === true) {
                            addAdminArray.push(index);
                        }
                    }
                    if (addAdminArray.length > 0) {
                        await fetch("/addAdmin/" + urlID, {
                            method: "post",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify({addAdminArray: addAdminArray}),
                        });
                        addAdminButton.innerHTML = /*html*/ ``;
                        groupInfo();
                    } else {
                        addAdminButton.innerHTML = /*html*/ ``;
                        for (let i = 0; i < addAdminCheckboxes.length; i++) {
                            addAdminCheckboxes[i].innerHTML = /*html*/ ``;
                        }
                        addAdminDecision = 0;
                        return;
                    }
                });
            }
            addAdminDecision = 1;
        } else if (addAdminDecision === 1) {
            addAdminButton.innerHTML = /*html*/ ``;
            for (let i = 0; i < addAdminCheckboxes.length; i++) {
                addAdminCheckboxes[i].innerHTML = /*html*/ ``;
            }
            addAdminDecision = 0;
        }
    });
}

async function waitMemberInfo() {
    const res = await fetch("/groupInfo/" + urlID);
    const result = await res.json();
    let memberInfo = result.groupInfo;
    const waitRes = await fetch("/groupWaitInfo/" + urlID);
    const waitResult = await waitRes.json();
    let waitInfo = waitResult.groupWaitInfo;

    groupWaitingMemberNumber.innerText = /*html*/ `${waitInfo.length}/250`;
    for (let i = 0; i < waitInfo.length; i++) {
        groupWaitingMemberList.innerHTML += /*html*/ `
		<div class="group-wait-member-info">
			<img class="group-member-icon" src="../uploads/${waitInfo[i].icon}">
			<div class="adminName">
				<div class="group-wait-member-name" index=${waitInfo[i].userId}>${waitInfo[i].display_name}</div>
				${
                    waitResult.admin
                        ? `<div class="accept-reject"><div class="accept-button btn btn-success">接受</div><div class="reject-button btn btn-danger">拒絕</div></div>`
                        : ""
                }
			</div>
		</div>`;
    }

    let acceptButtons = document.querySelectorAll(".accept-button");
    let rejectButtons = document.querySelectorAll(".reject-button");
    let groupWaitMembers = document.querySelectorAll(".group-wait-member-info");
    let groupWaitMemberNames = document.querySelectorAll(".group-wait-member-name");

    for (let i = 0; i < acceptButtons.length; i++) {
        let index = parseInt(groupWaitMemberNames[i].getAttribute("index"));
        acceptButtons[i].addEventListener("click", async function () {
            await fetch("/addToGroup/" + urlID, {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({index, index}),
            });
            groupWaitMembers[i].innerHTML = /*html*/ ``;
            groupInfo();
        });
        rejectButtons[i].addEventListener("click", async function () {
            await fetch("/deleteFromWaiting/" + urlID, {
                method: "delete",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({index, index}),
            });
            groupWaitMembers[i].innerHTML = /*html*/ ``;
            groupInfo();
        });
    }
}

// add events to these 2 buttons
recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);
async function changeGroupAsync() {
    const res = await fetch("/checkAdmin/" + urlID);
    const result = await res.json();
    if (result.is_admin) {
        changeGroupButton.setAttribute("href", `changegroup.html?id=${urlID}`);
    }
}

window.onload = async function () {
    const result = await checkInGroup();
    if (result.length > 0) {
        await findGroupName();
        await groupInfo();
        await waitMemberInfo();
        await getChatHistory();
        await changeGroupAsync();
    } else {
        window.location.href = "group.html";
    }
};
