import {Knex} from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("friends", (table) => {
        table.increments();
        table.integer("user_id").unsigned().notNullable();
        table.foreign("user_id").references("users.id");
        table.integer("friend_id").unsigned().notNullable();
        table.foreign("friend_id").references("users.id");
        table.timestamps(false, true);
        // unique
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("friends");
}
